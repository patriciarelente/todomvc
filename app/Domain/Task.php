<?php

namespace App\Domain;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    //
    protected $table = "tasks";

    protected $fillable = [
    	'id',
    	'task_name',
    	'status',
    ];
}
