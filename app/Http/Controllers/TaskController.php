<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Facades\Input;

use App\Http\Requests;
use App\Domain\Task;


class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //All
        $tasks = Task::orderby('task_name')->paginate();
        $activeTasks = Task::where('status', 0)->count();

        return view('index', [
                    'tasks'=>$tasks,
                    'taskCount'=>$activeTasks
                ]);
    }

    public function indexActive($request)
    {
        //Active
        if($request == 0){
            $tasks = Task::where('status', 0)
                    ->orderby('task_name')
                    ->paginate();
        }

        //Completed
        elseif($request == 1){
            $tasks = Task::where('status', 1)
                    ->orderby('task_name')
                    ->paginate();
        }

        //All
        else{
            $tasks = Task::orderby('task_name')->paginate();
            
        }
        
        $activeTasks = Task::where('status', 0)->count();

        return view('index', [
                    'tasks'=>$tasks,
                    'taskCount'=>$activeTasks
                ]);


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('../create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $getTask = $request->all();
        Task::create($getTask);

        return redirect()
                ->route('index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, $status)
    {
        //
        if($status == 0){
            $newStatus = 1;
            Task::where('id', $id)
                ->update(array('status' => $newStatus));
        }
        else{
            $newStatus = 0;
            Task::where('id', $id)
                ->update(array('status' => $newStatus));
        }

        return redirect()
                ->route('index');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update()
    {
        //
        $checkStatus = Task::where('status', 0)->get();

        if(!$checkStatus->isEmpty()){

            Task::orderby('task_name')
            ->update(array('status' => 1));
        }

        else{
            Task::orderby('task_name')
            ->update(array('status' => 0));
            
        }        

        return redirect()
            ->route('index');
    }

    public function editTask($id)
    {
        $editTask =  Task::find($id);
        // dd($editTask);
        return view('editTask',['tsk' => $editTask]);
    }

    public function updateTask(Request $request, $id)
    {
        // var_dump($request);
        // dd($id);
        Task::where('id', $id)
                ->update($request->only([
                    'task_name' ]));

        return redirect()
            ->route('index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function remove($id)
    {
        //can I use destroy here para mas maikli yung code?
        Task::where('id', $id)
            ->delete();

        return redirect()
                ->route('index');
    }

    public function clear()
    {
        //1 means completed task

         Task::where('status', 1)
                ->delete();

        return redirect()
                ->route('index');
    }
}
