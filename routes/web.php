<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });


Route::resource('/', 'TaskController');

//View Tasks
Route::get('index',[
	'uses' => 'TaskController@index'
]);

Route::get('index/{val}',[
	'uses' => 'TaskController@indexActive'
]);


//Create Tasks
Route::get('/create',[
	'uses' => 'TaskController@create',
	'as' => 'create'
]);

Route::post('store',[
	'uses' => 'TaskController@store'
]);


//Edit Tasks
Route::get('edit/{id}/{status}',[
	'uses' => 'TaskController@edit'
]);

Route::get('update',[
	'uses' => 'TaskController@update'
]);

Route::get('/editTask/{id}',[
	'uses' => 'TaskController@editTask',
	'as' => 'editTask'
]);

Route::put('/updateTask/{id}',[
	'uses' => 'TaskController@updateTask',
	'as' => 'updateTask'
]);


//Remove Tasks
Route::get('delete/{id}',[
	'uses' => 'TaskController@remove'
]);

Route::get('/clear',[
	'uses' => 'TaskController@clear'
]);