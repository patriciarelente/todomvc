<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>TODOMVC</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @if (Auth::check())
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ url('/login') }}">Login</a>
                        <a href="{{ url('/register') }}">Register</a>
                    @endif
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    todos
                </div>    
                <!-- Check Event Listeners -->
                @include('create')
                <table>
                    @foreach($tasks as $tsk)
                    <tr>
                        <td>
                            [<a href='edit/{{$tsk->id}}/{{$tsk->status}}' style="color:green; !important" >C</a>]

                            @if($tsk->status == 1)
                                <label style="text-decoration: line-through; !important">
                                    <a href=" {{route('editTask',$tsk->id)}} " style="text-decoration: none; !important">
                                        {{ $tsk->task_name }} 
                                    </a>
                                </label>
                             @else

                                <label>
                                    <a href=" {{route('editTask',$tsk->id)}} " style="text-decoration: none; !important">
                                        {{ $tsk->task_name }} 
                                    </a>
                                </label>
                            @endif

                            [<a href='delete/{{$tsk->id}}' style="color:red; !important">X</a>]
                        </td>
                    </tr>
                    @endforeach
                    <tr>
                        <td>{{ $taskCount }} items left</td>
                        <td>[<a href='../'>All</a>]</td>
                        <td>[<a href='../index/0'>Active</a>]</td>
                        <td>[<a href='../index/1'>Completed</a>]</td>
                        <td>
                            [<a href='../clear'>Clear Completed</a>]
                        </td>
                    </tr>
                </table>


            </div>
        </div>
    </body>
</html>
